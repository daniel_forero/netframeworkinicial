﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class SalaEmergencia: CuartoHospital
    {
        //Atributos
        private int numeroDeVentiladores;


       // Metodos

        public string mostrarCantidadDeventiladoresSobrantes()
        {
            // Numero de pacienes -numero de ventidadores

            // numeroDeVentiladores 10
            // CantidaPacientes 20
            // resultado = 10 -20 = -10
            int resultado = numeroDeVentiladores - CantidaPacientes;
            if(resultado < 0 || resultado == 0)
            {                                       
                return "No existen ventiladores sobrantes, faltan: " + resultado * -1;
            }

            return "La cantida de ventiladores disponibles es: " + resultado;
        }

        //get y sets



        public int NumeroDeVentiladores
        {
            get { return numeroDeVentiladores; }
            set { numeroDeVentiladores = value; }
        }

    }
}
